package com.telstra.codechallenge;

import com.intuit.karate.junit5.Karate;

public class FunctionalIT {
 
  @Karate.Test
  Karate testOldestUsers() {
    return Karate.run("oldestuser").relativeTo(getClass());
  }
}