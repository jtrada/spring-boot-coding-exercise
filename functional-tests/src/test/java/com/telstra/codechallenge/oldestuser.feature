# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to test the oldestuser uri

  Scenario: Is the oldestuser uri available and returning data
  	 Given url microserviceUrl
    And path '/oldestusers/'+ '2'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    * def quoteSchema = { login : '#string',  id : '#number', html_url : '#string' }
    And match response == '#[2] quoteSchema' 


