package com.telstra.codechallenge.oldestuser.service;

import java.util.List;

import com.telstra.codechallenge.oldestuser.model.Item;

public interface OldestUserService {
	List<Item> getOldestUsersZeroFollowers(int count);
}
