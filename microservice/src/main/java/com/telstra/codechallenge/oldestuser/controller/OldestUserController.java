package com.telstra.codechallenge.oldestuser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.oldestuser.model.Item;
import com.telstra.codechallenge.oldestuser.service.OldestUserService;
import com.telstra.codechallenge.oldestuser.service.OldestUserServiceImpl;


@RestController
@RequestMapping("/oldestusers")
public class OldestUserController {

	  private OldestUserService oldestUserService;
	  
	  
	@Autowired
	public OldestUserController(
			OldestUserServiceImpl oldestUserServiceImpl) {
		    this.oldestUserService = oldestUserServiceImpl;
		  }
	
	@GetMapping("/{number}")
	public List<Item> getOldestUsers(@PathVariable int number) {
		return oldestUserService.getOldestUsersZeroFollowers(number);
	}
	
}
