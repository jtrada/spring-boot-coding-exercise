package com.telstra.codechallenge.oldestuser.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.oldestuser.model.Item;
import com.telstra.codechallenge.oldestuser.model.User;

@Service
public class OldestUserServiceImpl implements OldestUserService {
	 @Value("${user.search.url}")
	  private String userSearchUrl;

	  private RestTemplate restTemplate;

	  public OldestUserServiceImpl(RestTemplate restTemplate) {
	    this.restTemplate = restTemplate;
	  }

	  @Override
	  public List<Item> getOldestUsersZeroFollowers(int count) {
		  
		 User users = restTemplate.getForObject(userSearchUrl+"?q=followers:0&sort=joined&order=asc", User.class);
		 return users.getItems().stream().limit(count).collect(Collectors.toList());
	  }

	 
}
