package com.telstra.codechallenge.oldestuser.model;

import org.junit.Test;

import org.junit.Assert;

public class ItemTest {
	@Test
	public void testLogin() {
		Item item = new Item();
		item.setLogin("vinayk");
		Assert.assertEquals(item.getLogin(),"vinayk");
	}
	@Test
	public void testId() {
		Item item = new Item();
		item.setId(123);
		Assert.assertEquals(item.getId(),123);
	}
	@Test
	public void testHtmlURL() {
		Item item = new Item();
		item.setHtml_url("https://github.com/errfree");
		Assert.assertEquals(item.getHtml_url(),"https://github.com/errfree");
	}
}
