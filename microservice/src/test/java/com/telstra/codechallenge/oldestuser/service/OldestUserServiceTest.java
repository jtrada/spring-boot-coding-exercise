package com.telstra.codechallenge.oldestuser.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.oldestuser.model.Item;
import com.telstra.codechallenge.oldestuser.model.User;


@RunWith(MockitoJUnitRunner.class)
@PropertySource("classpath:application.properties")
public class OldestUserServiceTest {

	@InjectMocks
	OldestUserServiceImpl oldestUserServiceImpl;

	@Mock
	RestTemplate restTemplate;
	
	@Value("${user.search.url}")
	  private String userSearchUrl;

	@Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testSizeGetOldestUsersZeroFollowers() {
		ArrayList<Item> testItems = new ArrayList<>();
		Item item = new Item();
		testItems.add(item);
		testItems.add(item);
		User user = new User();
		user.setItems(testItems);
		Mockito.when(restTemplate.getForObject(userSearchUrl+"?q=followers:0&sort=joined&order=asc", User.class)).thenReturn(user);
		List<Item> items = oldestUserServiceImpl.getOldestUsersZeroFollowers(2);
		Assert.assertEquals(items.size(), 2);
	}
	
	@Test
	public void testEmptyGetOldestUsersZeroFollowers() {
		ArrayList<Item> testItems = new ArrayList<>();
		User user = new User();
		user.setItems(testItems);
		Mockito.when(restTemplate.getForObject(userSearchUrl+"?q=followers:0&sort=joined&order=asc", User.class)).thenReturn(user);
		List<Item> items = oldestUserServiceImpl.getOldestUsersZeroFollowers(0);
		Assert.assertTrue(items.isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNegativeGetOldestUsersZeroFollowers() throws IllegalArgumentException{
		ArrayList<Item> testItems = new ArrayList<>();
		User user = new User();
		Item item = new Item();
		testItems.add(item);
		testItems.add(item);
		user.setItems(testItems);
		Mockito.when(restTemplate.getForObject(userSearchUrl+"?q=followers:0&sort=joined&order=asc", User.class)).thenReturn(user);
		List<Item> items = oldestUserServiceImpl.getOldestUsersZeroFollowers(-2);
	}
}
