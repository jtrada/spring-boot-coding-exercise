package com.telstra.codechallenge.oldestuser.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.telstra.codechallenge.oldestuser.model.Item;
import com.telstra.codechallenge.oldestuser.service.OldestUserService;

@RunWith(MockitoJUnitRunner.class)
public class OldestUserControllerTest {

	@InjectMocks
	OldestUserController oldestUserController;
	
	@Mock
	OldestUserService oldestUserService;
	
	 @Before
	    public void initMocks(){
	        MockitoAnnotations.initMocks(this);
	    }
	 
	@Test
	public void testSizeGetOldestUsersZeroFollowers() {
		ArrayList<Item> testItems = new ArrayList<>();
		Item item = new Item();
		testItems.add(item);
		testItems.add(item);
		Mockito.when(oldestUserService.getOldestUsersZeroFollowers(2)).thenReturn(testItems);
		List<Item> items = oldestUserController.getOldestUsers(2);
		Assert.assertEquals(items.size(), 2);
	}
	
	@Test
	public void testEmptyGetOldestUsersZeroFollowers() {
		ArrayList<Item> testItems = new ArrayList<>();
		Mockito.when(oldestUserService.getOldestUsersZeroFollowers(0)).thenReturn(testItems);
		List<Item> items = oldestUserController.getOldestUsers(0);
		Assert.assertTrue(items.isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNegativeGetOldestUsersZeroFollowers() throws IllegalArgumentException{
		Mockito.when(oldestUserService.getOldestUsersZeroFollowers(-2)).thenThrow(IllegalArgumentException.class);
		List<Item> items = oldestUserController.getOldestUsers(-2);
	}
}
