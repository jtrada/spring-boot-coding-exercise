package com.telstra.codechallenge.oldestuser.model;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class UserTest {
	@Test
	public void testTotalCount() {
		User user = new User();
		user.setTotal_count(3);
		Assert.assertEquals(user.getTotal_count(),3);
	}
	@Test
	public void testIncompleteResults() {
		User user = new User();
		user.setIncomplete_results(false);
		Assert.assertEquals(user.isIncomplete_results(),false);
	}
	@Test
	public void testItems() {
		User user = new User();
		ArrayList<Item> items = new ArrayList<>();
		Item item = new Item();
		items.add(item);
		user.setItems(items);
		Assert.assertSame(user.getItems(),items);
	}
}
